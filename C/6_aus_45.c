#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(){

    srand(time(0));
    char ziehung[6];
    char tipp[6];
    int f = 0;
    
    printf("Geben Sie 6 Zahlen ein: ");

    for(int i = 0; i < 6; i++){

        scanf("%d", &tipp[i]);
        
    }

    for(int i = 0; i < 6; i++){

        ziehung[i] = rand() % 45;
        
    }

    printf("Ihr Tipp: ");

    for(int y = 0; y < 6; y++){

        printf("%d ", tipp[y]);

    }

    printf("\nDie Ziehung: ");

    for(int z = 0; z < 6; z++){

       printf("%d ", ziehung[z]);

    }

    printf("\nÜbereinstimmende Zahlen: ");

    for(f = 0; f < 6; f++){

        if(ziehung[f] == tipp[f]){

            printf("%dte  ", f+1);
            
        }          
    }
}
