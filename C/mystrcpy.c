/**
 * file: mystrcpy.c
 *
 * date: 2020-11-04
 * progtimeest.: 15 min
 * progtimereal: 15 min 
 * author: Marco Brandauer
 * email: mbrandauer.its-b2020@fh-salzburg.ac.at
 *
 * Salzburg University of Applied Sciences
 * Information Technology & Systems Management
 * SWE1-ILV/B, exercise 3
 *
 */

#include <stdio.h>

int main(){

    char str[10000], str2[10000];
    int i = 0, ch = 0;

    printf("Geben Sie einen Text ein: ");

    fgets(str, 10000, stdin);

    while(i != sizeof(str)){
        str2[i] = str[i];
        i++;
    }

    printf("Der eingegebene Text war: %s\n", str);
    printf("Der kopierte Text ist: %s", str2);
}
