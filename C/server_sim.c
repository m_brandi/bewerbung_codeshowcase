#include <string.h>
#include <stdio.h>

unsigned long calculateHash(unsigned char *str);

int main(){

    unsigned char passwort[] = {"test1234"}; 
    unsigned char ben_eingabe[8];
    int hash_passwort = calculateHash(passwort);

    printf("Um auf unseren Server zugreifen zu können, müssen Sie das Kennwort eingeben: ");

    for(int i = 0; i < 3; i++){

        scanf("%s", &ben_eingabe);

        int hash_eingabe = calculateHash(ben_eingabe);

        if(hash_passwort == hash_eingabe){
            printf("Richtiges Kennwort\n");
            printf("\nWillkommen auf unserem Server!");
        }
        else{
            printf("Falsches Kennwort. Sie haben noch %d Versuch(e)\n", 2-i);
            if(i == 2){
                printf("Login_Time_Out");
            }
        }
    }
}

unsigned long calculateHash(unsigned char *str)
{
unsigned long hash = 5381;
int c;
while (c = *str++)
hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
return hash;
}
