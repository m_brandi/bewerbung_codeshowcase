#include <stdio.h>
#include <string.h>
#include <math.h>

int main(){

    int zahl, i = 0, f = 3, g = 0, a = 0;
    char rom[4000];
    char *i_rom[4000];
    int i_roman[] = { 1000,   900, 500, 400, 100,  90,  50,  40, 10,  9,   5,  4,   1 };
    char *s_roman[] = { "M" ,"CM" ,"D" ,"CD","C" ,"XC", "L","XL","X","IX","V","IV","I"};
    char st_el[30];

    printf("Geben Sie eine Zahl zwischen 1 und 9999 ein: ");
    scanf("%d", &zahl);

    while(f >= 0){
    
        rom[i] = zahl/pow(10, f);
        zahl = zahl - pow(10,f)*rom[i];
        i++;
        f--;

    }
    for(int y = 0; y < 4; y++){

        for(int z = 0; z < rom[y]; z++){

            if(rom[y] == 0){
                a += 1;
            }

            if(rom[y] < 4){
                i_rom[z+a] = s_roman[y*4];
            }
            else if(rom[y] >= 4 && y == 0  && y != 9){
                i_rom[z+a] = s_roman[0];
            }
            else if(rom[y] == 4 && y == 1){
                i_rom[z+a] = s_roman[3];
                z = rom[y];
                st_el[a] = 1;
                a -= 3;
            }
            else if(rom[y] == 4 && y > 0 && y != 1){
                i_rom[z+a] = s_roman[4*y-1];
                z = rom[y];
                st_el[a] = 1;
                a -= 3;
            }
            
            else if(rom[y] == 5 && y != 0){
                i_rom[z+a] = s_roman[4*y-2];
                z = rom[y];
                st_el[a] = 1;
                a -= 4;
            } 
            else if(rom[y] > 5 && rom[y] != 9){
                if(z == 0){
                i_rom[z+a] = s_roman[4*y-2];
                z += 4;
                a -= 4;
                }
                else{
                i_rom[z+a] = s_roman[y*4];
                }
                if(z == rom[y]-1){              
                }
            }
            else if(rom[y] == 9 && y == 0){
                i_rom[0] = s_roman[0];
            }       
            else if(rom[y] == 9 && y != 0 ){
               i_rom[z+a] = s_roman[4*(y-1)+1];
                z = rom[y];
                st_el[a] = 1;
                a -= 8;
            }
        }
        a += rom[y];
    }
    for(int t = 0; t < a; t++){

        if(st_el[t] == 0){
            printf("%c", *i_rom[t]);
        }
        else if(st_el[t] == 1){
            printf("%s", i_rom[t]);
        }      
    }
}
