#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define Range 5     //Menge der Zahlen

int list[Range];

void shuffle(){
    int z;
    for(int x = 1; x <= Range; x++){
        do{     
            z = rand() % Range;
        }
        while(list[z] != 0);
        list[z] = x;        
    } 
}
int check(){
    int x = 0;
    for(int i = 0; i < Range; i++){
       if(list[i] < list[i+1])
        x++;
    }
    if((x+1) == Range)
        return 1;
    else
        return 0;
}
void reset(){
    for(int i = 0; i < Range; i++)
        list[i] = 0;
}
int main(){
    srand(time(0));
    int ch = 0, counter = 0;
    while(ch == 0){  
        shuffle();
        for(int i = 0; i < Range; i++){
            printf("%d ", list[i]);
        }
        printf("\n");   
        ch = check();
        counter++;
        reset();
    }
    printf("\nZüge: %d\n", counter);
}
