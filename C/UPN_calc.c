#include <stdio.h>
#include <ctype.h>
#include <math.h>

float stack[1000], input[1000];
int pos = 999;
char ch;

void push(float el){
    stack[pos] = el;
    pos--;
}
float pop(){
    float el = stack[pos+1];
    stack[pos+1] = 0;
    pos++;
    return el;   
}
int empty(void){
    if(pos == 999){
        printf("\nStack is empty\n");
        return 1;
    }
    else{
        printf("\nStack is not empty\n");
        return 0;
    }
}
char top(void){
    if(empty() == 0)
    return stack[pos+1];
}
int size(void){
    int size = *(&stack+1) - stack;
    return size;
}
int length(void){
    return size()-(pos+1);
}
void reset(void){
    while((pos+1) <= size()){
        stack[pos+1] = 0;
        pos++;
    }
    pos = 999;
}
void input_c(int count, int count2, int check){
    if(check == 0){
        for(int i = 0; i < count; i++)
            input[i] *= pow(10, count-(i+1));                    
                
        for(int i = 0; i < count; i++){
            input[0] += input[i+1]; 
            input[i+1] = 0;           
        } 
    }
    else if(check == 1){        
        for(int i = 0; i < count; i++)
                input[i] *= pow(10, count-(i+1));                    

        for(int i = 1; i <= count2; i++)
                input[count+(i-1)] *= pow(10, -i);                    

        for(int i = 0; i < count+count2; i++){
                input[0] += input[i+1]; 
                input[i+1] = 0;           
        } 
    }
}
void clear(void){
    input[0] = 0;
}
int main(){

    int var = 0, counter = 0, counter2 = 0, op = 0, is_dec = 0;

    reset();

    int zahl;
    while((ch=fgetc(stdin)) != EOF){
        if(ch != '\n' && op == 0){
            if(isdigit(ch) == 1){
                input[counter+counter2] = ch-48;
                if(is_dec == 0)
                    counter++;

                else if(is_dec == 1)
                    counter2++;
            }
            if(ch == 46)
                is_dec = 1;
        }
        if(ch == 42 || ch == 43 || ch == 45 || ch == 47)
            op = ch;
        
        else if(ch == '\n'){
            input_c(counter, counter2, is_dec);
            counter = 0;

            if(op == 0){
                push(input[0]);
                input[0] = 0;
            }
            op = 0;
        }

        if(op != 0){
            if(op == 42){
                input[0] *= pop();
                printf("%.3f ", input[0]);
            }
            else if(op == 43){
                input[0] += pop();
                printf("%.3f ", input[0]);
            }
            else if(op == 45){
                input[0] = pop() - input[0];
                printf("%.3f ", input[0]);
            }
            else if(op == 47){
                input[0] = pop() / input[0];
                printf("%.3f ", input[0]);
            }           
        }       
    }        
}
