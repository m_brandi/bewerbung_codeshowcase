#include <stdio.h>

int fibonacci(int z);

int main(){

    int z;

    printf("Welches Element der Fibonacci-Folge möchten Sie wissen? ");

    scanf("%d", &z);

    printf("Das %d. Element der Fibonacci-Folge ist: %d", z, fibonacci(z));

    return 0;
}

int fibonacci(int z){

        if(z > 1){
        z = fibonacci(z-1) + fibonacci(z-2);
        }
}





