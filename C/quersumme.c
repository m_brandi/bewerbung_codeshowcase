#include <stdio.h>
#include <string.h>

int main(){

    char zahl[10000], str_element, q_sum, str_len;

    printf("Geben Sie eine Zahl ein: ");
    scanf("%s", &zahl);

    str_len = strlen(zahl);

    while(str_len > 0){

        q_sum += zahl[str_element]-48;
        str_element++;
        str_len--;
    }

    printf("Die Quersumme ist: %d", q_sum);

    return 0;

}
