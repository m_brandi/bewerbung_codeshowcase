/**
 * file: pyramid.c
 *
 * date: 2020-11-04
 * progtimeest.: 10 min
 * progtimereal: 10 min 
 * author: Marco Brandauer
 * email: mbrandauer.its-b2020@fh-salzburg.ac.at
 *
 * Salzburg University of Applied Sciences
 * Information Technology & Systems Management
 * SWE1-ILV/B, exercise 4
 *
 */

#include <stdio.h>

int main(){

    char *star[] = {"*","***", "*****", "*******"};
    int y = 0;

    for(int i = 0; i < 4; i++){
        while(y < 6-i){
            printf(" ");
            y++;
        }
        printf("%s", star[i]);
        printf("\n");
        y = 0;
    }

}
