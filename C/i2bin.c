#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

int main(){

    int zahl, i = 0, f = 0, y = 0;
    char bin[10000];
    bool is_div = true;


    printf("Geben Sie eine Zahl ein: ");
    scanf("%d", &zahl);

    while (is_div == true){
        if(zahl < pow(2, f)){
            is_div = false;
        }
        else{
            is_div = true;
        }
        f++;
    }
    
    if(zahl == 0){
        bin[0] = 0;
        f = 2;
    }

    while(zahl != 0){

        bin[i] = zahl%2;
        zahl /= 2;
        i++;
    }
    
    
    printf("In binärer Darstellung: ");

    while(f-1 > 0){

    printf("%d", bin[f-2]);
        f--;
    }

}
