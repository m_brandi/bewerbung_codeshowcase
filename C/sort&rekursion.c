#include<stdio.h>
#include<stdlib.h>

void sort(int *ar, int size);
void zahlenfolge(int n);
int summe(int n);
int fakultaet(int n);

int main(){
    
    int ar[10] = {1,6,2,8,43,20,11,37,54,60};
    sort(ar, 10);
    printf("Sortiertes Array: \n");
    for(int i = 0; i < (sizeof(ar)/sizeof(int)); i++){
        printf("%d\n", ar[i]);
    }
    printf("Zahlenfolge: \n");
    zahlenfolge(10);
    printf("Summe: %d\n", summe(10));
    printf("Fakultät: %d", fakultaet(5));
    return 0;
}

void sort(int *ar, int size){
    int kl_zahl = INT_MAX, pos = 0, temp;

    for(int z = 0; z < size; z++){
        for(int i = z; i < size; i++){
            if(ar[i] < kl_zahl){
                kl_zahl = ar[i];
                pos = i;
            }
        }
        temp = ar[z];
        ar[z] = kl_zahl;
        kl_zahl = INT_MAX;
        ar[pos] = temp;
    }
}
void zahlenfolge(int n){
  
    if(n > 1){
        zahlenfolge(n-1);
    }
    printf("%d\n", n);
}
int summe(int n){
     int sum = 0;
     if(n > 0)
        sum = n+summe(n-1);

    return sum;
 }

int fakultaet(int n){
    int f = 1;

    if(n > 1){
        f = n * fakultaet(n-1);
    }
}


