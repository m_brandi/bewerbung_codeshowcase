#include <stdio.h>
#include <stdlib.h>
//#include <wiringPi.h>

char alph[41] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',1,2,3,4,5,6,7,8,9,0,'Ä','Ö','Ü','.',' '};
static const char* mors[41] = {".-", "-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--..",".----","..---","...--","....-",".....","-....","--...","---..","----.","-----",".-.-","---.","..--", ".-.-.-", " "};
static const char* text[1000];

char stringToCh(char *ar);
int main(){
   char ch;
   int pos, ctr = 0, ctr2 = 0;

   //if(wiringPiSetup() == -1)
      //return 1;

   //pinMode(0, OUTPUT);

   printf("Nachricht (in Großbuchstaben): ");
   while((ch = fgetc(stdin)) != 10){
      for(int i = 0; i < 41; i++){
         if(alph[i] == ch)
            pos = i;
      }
      text[ctr] = mors[pos];
      ctr++;
   }

   while(text[ctr2] != '\0'){
      printf("%s ", text[ctr2]);
      ctr2++;
   }
   ctr = 0;
   ctr2 = 0;
   printf("\n");
   while(text[ctr] != '\0'){
      while(text[ctr][ctr2] != '\0'){
         //switch(text[0][ctr2]){
         //case '.': digitalWrite(0, HIGH); delay(400); digitalWrite(0, LOW); delay(200); break;
         //case '-': digitalWrite(0, HIGH); delay(800); digitalWrite(0, LOW); delay(200); break;
         //case ' ': delay(400); break;
         printf("%c", text[ctr][ctr2]);
         ctr2++;
      }
      //delay(900);
      printf("\n");
      ctr2 = 0;
      ctr++;
   }
   return 0;
}
