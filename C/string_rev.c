#include <stdio.h>
#include <string.h>

int main(){

    char string[10000], string2[10000];
    int i = 0, counter = 0;

    printf("Geben Sie eine Zeichenkette ein: ");

    fgets(string, 10000, stdin);

    

    while(string[i] != '\0'){                           //Wenn man den Befehl "strrev" nicht verwenden will
        counter++;
        i++;
    }
    i = 0;
    while(i < counter){
        string2[i] = string[(counter-1)-i];
        i++;
    }

    printf("%s\n", string2);

    //ODER

    printf("%s\n", strrev(string));  

    return 0;

}
