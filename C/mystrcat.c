/**
 * file: mystrcat.c
 *
 * date: 2020-11-04
 * progtimeest.: 15 min
 * progtimereal: 15 min 
 * author: Marco Brandauer
 * email: mbrandauer.its-b2020@fh-salzburg.ac.at
 *
 * Salzburg University of Applied Sciences
 * Information Technology & Systems Management
 * SWE1-ILV/B, exercise 4
 *
 */

#include <stdio.h>

int main(){

    char str1[20000], str2[10000];
    int str1_len = 0, i = 0;

    printf("Geben Sie einen Text ein: ");
    fgets(str1, 10000, stdin);

    printf("Und nun den Text den Sie anhängen wollen: ");
    fgets(str2, 10000, stdin);

    while(str1[i] != '\n'){
        str1_len++;
    }

    while(i < sizeof(str2)){
        str1[str1_len+i] = str2[i];
        i++;
    }
    printf("Der zusammengefügte Text: %s", str1);



}
