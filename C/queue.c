#include <stdio.h>

int fifo[1000];
int pos = 999;

void fifo_in(int value){
    fifo[pos] = value;
    pos--;
}

int empty(void){
    if(fifo[999] == '\0')
        printf("\nStorage is empty\n");
        
    else{
        printf("\nStorage is not empty\n");
    }
}
int size(void){
    int size = *(&fifo+1) - fifo;
    return size;
}
int length(void){
    int i = 0;
    while(fifo[999-i] != '\0')
        i++;
   
    return i;
}
void correct(void){
    for(int i = 0; i < length(); i++)
        fifo[999-i] = fifo[998-i];   
}
int fifo_out(void){
    int i = 0;
    i = fifo[999];
    correct();
    return i;
}
void reset(void){
    while((999-length()) <= (size()-2))
        fifo[1000-length()] = 0;
    
    pos = 999;
}

int main(){

    empty();
    printf("\nPossible storage size: %d\n", size());
    for(int i = 1; i <= 20; i++)
        fifo_in(i);

    for(int i = 0; i < 7; i++)
    printf("\n%d\n", fifo_out());
    
    empty();

    printf("\nThere are currently %d elements in the storage\n", length());

    reset();

    empty();

    printf("\nThere are currently %d elements in the storage\n", length());

    for(int i = 0; i < 5; i++)
        printf("\n%d\n", fifo_out());
}
