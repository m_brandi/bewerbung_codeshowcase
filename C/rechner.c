#include <stdio.h>

int main(){

    float z1, z2, result;
    char r_op;

    printf("Geben Sie 2 Zahlen ein: ");

    scanf("%f", &z1);
    scanf("%f", &z2);
    fflush(stdin);
    result = z1 + z2;

    printf("Die Summe von %.0f und %.0f = %.0f\n", z1, z2, result);

    printf("Geben Sie nun einen gültigen Operator ein (*, /, -): ");

    scanf("%c", &r_op);
    fflush(stdin);

    if(r_op == '*'){
        result = z1*z2;
    }else if(r_op == '/'){
        result = z1/z2;
    }else if(r_op == '-'){
        result = z1 - z2;
    }
    else{
        result = NULL;
        printf("\nDie Eingabe war ungültig\n");
    }


    if(result != NULL)
    printf("\nDrücken Sie 'Enter' um das Ergebnis zu bekommen\n");
    
    while(fgetc(stdin) != '\n'){

    }
      
    printf("Das Ergebnis lautet: %.2f", result);

    return 0;
  
}
