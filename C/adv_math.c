#include <stdio.h>
#include <math.h>
#include <time.h>
#include <complex.h>

int main(){

    srand(time(0));

    int antwort;
    int x = rand() % 6;
    int z1 = rand() % 5;
    int z2 = rand() % 5;

    printf("Was ist %d hoch %d?\n", z1, z2);
    printf("Antwort: ");
    scanf("%d", &antwort);

    if(antwort == pow(z1, z2)){
        printf("\nRichtig!");
    }
    else{
        printf("Leider falsch");
    }

    printf("\nUnd was ist log10 von 10^%d?\n", x);
    scanf("%d", &antwort);

    if (antwort == log10(pow(10, x)))
    {
       printf("\nRichtig!");
    }
    else{
        printf("Leider falsch");
    }
    
    printf("\nUnd weißt du was ln von e^3 ist? ");
    scanf("%d", &antwort);

    if(antwort == log(exp(3))){
        printf("\nRichtig");
    }
    else{
        printf("\n Leider falsch");
    }
}
