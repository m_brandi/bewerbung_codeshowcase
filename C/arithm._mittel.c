#include <stdio.h>

int main(){

    float zahlen, sum;

    printf("Geben Sie 10 Zahlen ein, wovon der Durchschnitt berechnet werden sollte: ");

    for(int i = 0; i < 10; i++){

        scanf("%f", &zahlen);            
        sum += zahlen;
    }

    sum /= 10;

    printf("Der Durschnitt liegt bei: %.2f", sum);
       
    return 0;

}
