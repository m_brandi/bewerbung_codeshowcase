/**
 * file: mystrlen.c
 *
 * date: 2020-11-04
 * progtimeest.: 10 min
 * progtimereal: 5 min 
 * author: Marco Brandauer
 * email: mbrandauer.its-b2020@fh-salzburg.ac.at
 *
 * Salzburg University of Applied Sciences
 * Information Technology & Systems Management
 * SWE1-ILV/B, exercise 3
 *
 */

#include <stdio.h>

int main(){

    char src[10000];
    int counter = 0, i = 0;

    printf("Geben Sie einen Text ein: ");

    while(fgetc(stdin) != '\n')
        counter++;

    printf("Der eingegebene Text ist %d Zeichen lang", counter);

}
