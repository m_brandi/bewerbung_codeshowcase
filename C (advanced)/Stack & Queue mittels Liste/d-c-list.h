/**
 * file: d-c-list.h
 *
 * date: 2021-05-24
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#ifndef D_C_LIST_H
#define D_C_LIST_H

typedef struct link{
    double value;
    struct link *prev, *next;
}LINK;

void create_list(int size);
int insert_front(double val);
void insert_back(double val);
double val_at_pos(int pos);
int isListEmpty();
int delete_front();
int delete_back();
int delete_list();

#endif //D_C_LIST_H
