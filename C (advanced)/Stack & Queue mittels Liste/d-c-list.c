/**
 * file: d-c-list.c
 *
 * date: 2021-05-24
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#include <stdio.h>
#include "d-c-list.h"
#include <stdlib.h>
#include <limits.h>

LINK *list = NULL;
int list_max_size = 0;

void create_list(int size){
    list = malloc(sizeof(LINK));
    list_max_size = size;

    list -> value = 0;
    list -> prev = NULL;
    list -> next = NULL;
    return;
}
int insert_front(double val){
    if(list == NULL){
        return 1;
    }
    if(val > LONG_MAX || val < LONG_MIN){
        fprintf(stderr, "Wert außerhalb des g\x81ltigen Wertebereichs (double)!\n");
        return 2;
    }
    if(list -> value == list_max_size && list_max_size != 0){
        return 3;
    }
    LINK *front = list -> prev;
    LINK *new_link = malloc(sizeof(LINK));
    new_link -> value = val;
    if(front == NULL){
        list->next = new_link;
        list->prev = new_link;
        list -> value++;
        return 0;
    }
    else{
        front -> prev = new_link;
        list -> prev = new_link;
        list -> value++;
        new_link -> next = front;
        return 0;
    }
}
void insert_back(double val){
    if(list == NULL){
        fprintf(stderr, "Keine existierende Liste!\n");
        return;
    }
    if(val > LONG_MAX || val < LONG_MIN){
        fprintf(stderr, "Wert außerhalb des g\x81ltigen Wertebereichs (double)!\n");
        return;
    }
    if(list -> value == list_max_size && list_max_size != 0){
        fprintf(stderr, "Maximale Listengr\x94\xe1""e bereits erreicht!\n");
        return;
    }

    LINK *tail = list -> next;
    LINK *new_link = malloc(sizeof(LINK));
    new_link -> value = val;

    if(tail == NULL) {
        list->next = new_link;
        list->prev = new_link;
        list -> value++;
        return;
    }
    else{
        tail -> next = new_link;
        list -> value++;
        new_link -> prev = tail;
        list -> next = new_link;
        return;
    }
}
double val_at_pos(int pos){
    if(list == NULL){
        return -1.0;
    }
    if(pos < 0 || pos > list_max_size){
        fprintf(stderr, "Ung\x81ltige Position\n");
        return -2.0;
    }
    if(pos == 0){
        pos = list -> value;
    }
    LINK* link = list -> prev;
    if(link == NULL){
        return -3.0;
    }
    for(int i = 1; i < pos; i++){
        link = link -> next;
    }
    return link -> value;
}
int isListEmpty(){
    if(list == NULL){
        fprintf(stderr, "Keine existierende Liste!\n");
        return -1;
    }
    if(list -> value == 0){
        return 1;
    }
    else{
        return 0;
    }
}
int delete_front(){
    if(list == NULL){
        fprintf(stderr, "Keine existierende Liste!\n");
        return -1.0;
    }
    LINK *front = list -> prev;
    if(front == NULL){
        //fprintf(stderr, "Liste ist leer!\n");
        return -2.0;
    }
    if(list -> value == 1){
        list -> prev = NULL;
        list -> next = NULL;
        printf("Wert: %.2lf\n", front -> value);
        free(front);
        return 0.0;
    }
    front -> next -> prev = NULL;
    list -> prev = front -> next;
    printf("Wert: %.2lf\n", front -> value);
    free(front);
    list -> value--;

    return 0.0;
}
int delete_back(){
    if(list == NULL){
        //fprintf(stderr, "Keine existierende Liste!\n");
        return 1;
    }
    LINK *back = list -> next;
    if(back == NULL){
        //fprintf(stderr, "Liste ist leer!\n");
        return 2;
    }
    if(list -> value == 1){
        list -> prev = NULL;
        list -> next = NULL;
        free(back);
        return 0;
    }
    back -> prev -> next= NULL;
    list -> next = back -> prev;
    printf("Wert: %.2lf\n", back -> value);
    free(back);
    list -> value--;

    return 0;
}
int delete_list(){
    int error = 0;
    if(list == NULL){
        fprintf(stderr, "Keine existierende Liste!\n");
        return error;
    }
    int len = list -> value;
    if(len > 0) {
        for (int i = 0; i < len; i++) {
            if(delete_front(list) == -2.0){
                error = 1;
            }

        }
    }
    free(list);
    list = NULL;
    return error;
}
