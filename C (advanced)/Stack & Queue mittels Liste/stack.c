/**
 * file: stack.c
 *
 * date: 2021-05-24
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#include <stdio.h>
#include "d-c-list.h"
#include "stack.h"

void create_stack(int size){
    create_list(size);
}
void push(double val){
    insert_back(val);
}
void pop(){
    switch(delete_back()){
        case 1:
            fprintf(stderr, "Keine existierende Queue!\n"); return;
        case 2:
            fprintf(stderr, "Queue ist leer!\n"); return;
        case 0:
            break;
    }
}
void isEmpty(){
    if(isListEmpty() == 1){
        printf("Stack ist leer!\n");
    }
    else{
        printf("Stack ist nicht leer!\n");
    }
}
void top(){
    printf("Oberster Wert: %.2lf\n", val_at_pos(0));
}
void delete_stack(){
    int stat = delete_list();
    if(stat == 1){
        fprintf(stderr, "Stack ist leer!\n");
    }
}
