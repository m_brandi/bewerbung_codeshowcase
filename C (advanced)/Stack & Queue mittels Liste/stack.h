/*
 * file: stack.h
 *
 * date: 2021-05-24
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#ifndef STACK_H
#define STACK_H

void create_stack(int size);
void push(double val);
void pop();
void isEmpty();
void top();
void delete_stack();

#endif //STACK_H
