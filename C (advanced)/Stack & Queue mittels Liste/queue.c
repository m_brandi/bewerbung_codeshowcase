/**
 * file: queue.c
 *
 * date: 2021-05-24
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#include <stdio.h>
#include "queue.h"
#include "d-c-list.h"

void create_queue(int size){
    create_list(size);
}
void enqueue(double val){
    switch(insert_front(val)){
        case 1:
            fprintf(stderr, "Keine existierende Queue!\n"); return;
        case 2:
            return;
        case 3:
            fprintf(stderr, "Maximale Queue-Gr\x81\x94""e erreicht!\n");
        case 0:
            break;
    }
}
void dequeue(){
    switch(delete_back()){
        case 1:
            fprintf(stderr, "Keine existierende Queue!\n"); return;
        case 2:
            fprintf(stderr, "Queue bereits leer!\n"); return;
        case 0:
            break;
    }
}
void front(){
    if(val_at_pos(0) == -1.0){
        fprintf(stderr, "Keine existierende Queue!\n");
        return;
    }
    if(val_at_pos(0) == -3.0){
        fprintf(stderr, "Queue ist leer!\n");
        return;
    }
    printf("Vorderer Wert: %.2lf\n", val_at_pos(0));
}
void rear(){
    printf("Hinterer Wert: %.2lf\n", val_at_pos(1));
}
void delete_queue(){
    int stat = delete_list();
    if(stat == 1){
        fprintf(stderr, "Queue ist leer!\n");
    }
}
