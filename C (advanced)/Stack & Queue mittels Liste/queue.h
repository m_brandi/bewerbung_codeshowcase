/**
 * file: queue.h
 *
 * date: 2021-05-24
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#ifndef QUEUE_H
#define QUEUE_H

void create_queue(int size);
void enqueue(double val);
void dequeue();
void front();
void rear();
void delete_queue();

#endif //QUEUE_H
