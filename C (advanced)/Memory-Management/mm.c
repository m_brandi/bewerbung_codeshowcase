/**
 * file: mm.c
 *
 * date: 2021-05-20
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "SingleLinkedList.h"
#include "mm.h"

/****************
* f = First Fit *
* n = Next Fit  *
* w = Worst Fit *
****************/

NODE * head = NULL;
void * mem = NULL;
NODE * global_pos = NULL;
char global_strat;
int mem_size;

void change_strat(char str){
    global_strat = str;
}
void * mminit(size_t size, char strat){
    if(size >= 10000000){
        fprintf(stderr, "Die gewählte Speicherblockgr\x94" "ße ist zu groß!\n");
        return NULL;
    }
    if(size < 1){
        fprintf(stderr, "Ung\x81" "ltige Speichergr\x94" "ße!\n");
        return NULL;
    }
    switch (strat){
        case 'f':
            global_strat = 'f'; break;
        case 'w':
            global_strat = 'w'; break;
        case 'n':
            global_strat = 'n'; break;
        default:
            fprintf(stderr, "Ung\x81" "ltiger Parameter (f, w, n)!\n"); return NULL;
    }
    mem_size = size;
    mem = malloc(size);
    if (NULL == mem){
        return NULL;
    }
    head = calloc(1, sizeof(NODE));
    if (NULL == head){
        fprintf(stderr, "Speicher konnte nicht reserviert werden!\n");
        free(mem);
        return NULL;
    }
    return mem;
}

void mmterm(){
    if(mem != NULL) {
        free(mem);
        mem = NULL;
    }
    if(head != NULL) {
        deleteList(head->nxt);
        free(head);
        head = NULL;
    }
    if(head == NULL){
        fprintf(stderr, "Keine existierende Referenz zu einer Liste\n");
    }
}

void * mmmalloc(int size, char * name){
    if(size > mem_size){
        fprintf(stderr, "Speicherblock zu groß!\n");
        return NULL;
    }
    if(name == NULL){
        name = '\0';
    }
    NODE * new_node = createNode(name, size);
    if (NULL == new_node)
        return NULL;
    if (global_strat == 'f'){
        // mem ist leer -> am Anfang einfuegen
        if(head->nxt == NULL){
            head->nxt = new_node;
            new_node->start_of_block = mem;
            global_pos = head -> nxt;
            return new_node->start_of_block;
        } else {
            NODE * prev = head->nxt;
            for (NODE * nd = head->nxt; nd != NULL; nd = nd->nxt){
                // Am Anfang ist ausreichend Platz -> am Anfang einfuegen
                if (head->nxt->start_of_block != mem){
                    // ist da eine Lücke die groß genug ist?
                    if (size <= head -> nxt -> start_of_block - mem){
                        new_node->start_of_block = mem;
                        new_node->nxt = head->nxt;
                        head->nxt = new_node;
                        return new_node->start_of_block;
                    }
                }
                // Ist in der Mitte eine Lücke?
                if (nd->start_of_block - (prev->start_of_block + prev->size) >= size){
                    new_node->start_of_block = prev->start_of_block + prev->size;
                    prev->nxt = new_node;
                    new_node->nxt = nd;
                    return new_node->start_of_block;
                }
                // Ist Am Ende eine Lücke?
                if ((nd->nxt == NULL) && (mem + mem_size - (nd->start_of_block + nd->size) >= size)){
                    nd->nxt = new_node;
                    new_node->start_of_block = nd->start_of_block + nd->size;
                    return new_node->start_of_block;
                }
                prev = nd;
            }
            // mem ist voll
            return NULL;
        }
    } else if (global_strat == 'w') {
        // mem ist leer -> am Anfang einfuegen
        if(head->nxt == NULL){
            head->nxt = new_node;
            new_node->start_of_block = mem;
            global_pos = head -> nxt;
            return new_node->start_of_block;
        } else {
            NODE * prev = head->nxt;
            NODE * nd = head->nxt;
            NODE * pos = NULL;
            int sizeOfSpace = nd -> start_of_block - mem;  //Speichert Lückengröße
            //Überprüft, ob Lücke am Ende groß genug ist und Liste nur 1 Element hat
            if((mem + mem_size) - (prev -> start_of_block + prev -> size) >= size && head -> nxt -> nxt == NULL) {
                prev-> nxt = new_node;
                new_node -> start_of_block = prev -> start_of_block + prev -> size;
                return new_node -> start_of_block;
            }
            nd = nd->nxt;
            while (nd != NULL) {
                if (nd->start_of_block - (prev->start_of_block + prev->size) > sizeOfSpace){
                    sizeOfSpace = nd->start_of_block - (prev->start_of_block + prev->size);
                    pos = prev;
                }
                prev = nd;
                nd = nd -> nxt;
            }
            //Ist die bisherige größte gefunde Lücke oder die Lücke zw. letztem Lement und Ende groß genug?
            if(sizeOfSpace >= size || (mem + mem_size) - (prev -> start_of_block + prev -> size) >= sizeOfSpace) {
                //Ist Lücke am Ende größer als bisherige größte Lücke?
                if((mem + mem_size) - (prev -> start_of_block + prev -> size) >= sizeOfSpace) {
                    prev -> nxt = new_node;
                    new_node -> start_of_block = prev -> start_of_block + prev -> size;
                }
                //Ist Lücke am Anfang größer als bisher größte Lücke?
                else if(sizeOfSpace <= head -> nxt -> start_of_block - mem) {
                    new_node -> nxt = head -> nxt;
                    new_node -> start_of_block = mem;
                    head -> nxt = new_node;
                } else {
                    new_node -> nxt = pos -> nxt;
                    pos -> nxt = new_node;
                    new_node -> start_of_block = pos -> start_of_block + pos -> size;
                }
                return new_node -> start_of_block;
            }
            else{
                fprintf(stderr, "\nFehler mit Zeiger %s\n", name);
                return NULL;
            }
        }
    } else if (global_strat == 'n') {
        //Ist Liste leer und Zeiger noch nicht gesetzt?
        if(head->nxt == NULL && global_pos == NULL) {
            head->nxt = new_node;
            new_node->start_of_block = mem;
            global_pos = head -> nxt;     //Speichert Position des zuletzt allokierten Blockes
            return new_node->start_of_block;
        } else {
            NODE *prev = global_pos;
            NODE *nd = global_pos->nxt;
            //Wiederholt so lange, bis wieder an Anfangsposition (nicht Anfang des Heaps/Speichers) zurück ist
            while (nd != global_pos) {
                //Ist globale Position am Ende des Speichers?
                if (nd == NULL) {
                    nd = head->nxt;
                    //Ist Lücke am Ende groß genug?
                    if ((mem + mem_size) - (prev->start_of_block + prev->size) >= size && prev->nxt == NULL) {
                        prev->nxt = new_node;
                        new_node->start_of_block = prev->start_of_block + prev->size;
                        global_pos = new_node;
                        return new_node->start_of_block;
                    }
                    //Ist Lücke am Anfang groß genug?
                    else if (size <= head->nxt->start_of_block - mem) {
                        new_node->nxt = head->nxt;
                        new_node->start_of_block = mem;
                        head->nxt = new_node;
                        global_pos = new_node;
                        return new_node->start_of_block;
                    }
                    prev = nd;
                    nd = nd->nxt;
                }
                //Ansonsten in die Mitte einfügen
                else {
                    if (nd->start_of_block - (prev->start_of_block + prev->size) >= size) {
                        new_node->start_of_block = prev->start_of_block + prev->size;
                        prev->nxt = new_node;
                        new_node->nxt = nd;
                        global_pos = new_node;
                        return new_node->start_of_block;
                    }
                }
                prev = nd;
                nd = nd->nxt;
            }
            fprintf(stderr, "Fehler beim Zeiger %s\n", name);
        }
    }
}

void mmfree(void * start){
    if(start == NULL){
        fprintf(stderr, "Ung\x81" "ltiger Zeiger\n");
        return;
    }
    NODE * prev = head->nxt;
    NODE * nd = head->nxt;
    while (nd != NULL){
        if (nd->start_of_block == start){
            if (head->nxt == nd){ // erstes Element
                head->nxt = nd->nxt;
            } else if (nd->nxt == NULL){ // letztes Element
                prev->nxt = NULL;
            } else {    // mittleres Element
                prev->nxt = nd->nxt;
            }
            free(nd);
        }
        prev = nd;
        nd = nd->nxt;
    }
}

void mmrename(char * start, char * new_name){
    if(start == NULL){
        fprintf(stderr, "Ung\x81" "ltiger Zeiger\n");
        return;
    }
    if(new_name == NULL){
        new_name = '\0';
    }
    NODE * nd = head->nxt;
    while (nd != NULL){
        if (nd->start_of_block == start){
            strncpy(nd->name, new_name, sizeof(nd->name));
        }
        nd = nd->nxt;
    }
}

void mmdump(){
    NODE * nd;
    char pz = '%';  //zum einfachen schreiben bei printf
    int block_ind = 0, block_size = 0, free_nr = -1;

    for (nd = head->nxt; nd != NULL; nd = nd->nxt){
        block_ind++;
        block_size = block_size + nd->size;
        if (nd->nxt != NULL && (nd->start_of_block + nd->size) != nd->nxt->start_of_block)
            free_nr++;
    }
    if (head->nxt != NULL)
        free_nr++;
    for (nd = head->nxt; nd->nxt != NULL; nd = nd->nxt); // gehe zum letzten Element
    if (mem + mem_size != nd->start_of_block + nd->size)
        free_nr++;

    float prozent_belegt = (float)block_size/(float)mem_size * 100;

    printf("memsize: %d\n", mem_size);
    printf("Belegte Bl\x94" "cke: %d (%d Byte / %d%c)\n", block_ind, block_size, (int)prozent_belegt, pz);
    printf("Freie Bl\x94" "cke: %d (%d Byte / %d%c)\n\n", free_nr, mem_size - block_size, 100 - (int)prozent_belegt, pz);
    printf("Startadresse\tGr\x94\xe1" "e\tStatus\tName\n");
    if (head->nxt != NULL){
        if(head->nxt->start_of_block != mem){
            printf("0x0000:%04d\t%d\tfrei\t-\n", mem - mem, head->nxt->start_of_block - mem);
        }
        for (nd = head->nxt; nd->nxt != NULL; nd = nd->nxt){
            printf("0x0000:%04d\t%d\tbelegt\t%s\n", nd->start_of_block - mem, nd->size, nd->name);
            if (nd->start_of_block + nd->size != nd->nxt->start_of_block){
                printf("0x0000:%04d\t%d\tfrei\t-\n", (nd->start_of_block + nd->size) - mem, nd->nxt->start_of_block - (nd->start_of_block + nd->size));
            }
        }
        printf("0x0000:%04d\t%d\tbelegt\t%s\n", nd->start_of_block - mem, nd->size, nd->name);
        if(mem + mem_size != nd->start_of_block + nd->size){
            printf("0x0000:%04d\t%d\tfrei\t-\n", (nd->start_of_block + nd->size) - mem, (mem + mem_size) - (nd->start_of_block + nd->size));
        }
    }
}
