/*
 * file: SingleLinkedList.h
 *
 * date: 2021-05-20
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#ifndef SINGLELINKEDLIST_H_
#define SINGLELINKEDLIST_H_


typedef struct Node {
    char name[10];
    int size; // in Byte
    void * start_of_block;
    struct Node * nxt;
} NODE;

struct Node* createNode(char* name, int size);
void deleteList(struct Node* list);


#endif // SINGLELINKEDLIST_H_
