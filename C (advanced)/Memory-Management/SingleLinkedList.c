/*
 * file: SingleLinkedList.c
 *
 * date: 2021-05-20
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "SingleLinkedList.h"

/* Erzeugt einen neuen Knoten mit fesgelegtem Namen und Größe */
struct Node* createNode(char* name, int size) {
    if(name == NULL){
        name = '\0';
    }
    if(size > INT_MAX){
        fprintf(stderr, "Gr\x94" "\xe1" "e \x81" "berschreitet das Maximum\n");
        return NULL;
    }
    if(size < 1){
        printf("Ung\x81" "ltige Gr\x94" "\xe1 " "e\n");
        return NULL;
    }
    NODE * nd = calloc(1, sizeof(NODE));
    if (NULL == nd){
        fprintf(stderr, "Failed allocating memory!\n");
        return NULL;
    }
    strncpy(nd->name, name, sizeof(nd->name));  //strncopy um mögliche Stringlänge zu übergeben
    nd->size = size;
    return nd;
}

/* Löscht die ganze Liste*/
void deleteList(struct Node* list) {
    if(list == NULL){
        fprintf(stderr, "Keine Liste \x81" "bergeben\n");
        return;
    }
    struct Node * nd = list, * tmp;
    while(nd -> nxt != NULL) {
        tmp = nd;
        free(tmp);
    }
}
