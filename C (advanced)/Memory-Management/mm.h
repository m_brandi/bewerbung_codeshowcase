/**
 * file: mm.h
 *
 * date: 2021-05-20
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#ifndef MM_H_
#define MM_H_


void change_strat(char str);
/* Ändert die Belegungsstrategie*/

void * mminit(size_t size, char strat);
/* Initialisiert einen Heap mit einer beliebigen Größe, legt die internen
   Verwaltungsstrukturen an und bestimmt die Belegungsstrategie. */


void mmterm();
/* Gibt den Heap und alle Verwaltungsstrukturen wieder frei. */


void * mmmalloc(int size, char * name);
/* Allokiert auf dem Heap einen Speicherblock mit einer beliebigen Größe;
   optional sollte über einen zusätzlichen Parameter noch ein Blockname 
   angegeben werden können. Die Funktion liefert die Startadresse des 
   Speicherblocks. */


void mmfree(void*);
/* Gibt einen bestimmten Speicherblock, adressiert über die Startadresse
   wieder frei. Nebeneinanderliegende freie Blöcke sollten zu einem größeren
   freien Speicherblock vereint werden. */


void mmrename(char * old_name, char * new_name);
/* Setzt den Namen eines belegten Speicherblocks; nimmt hierzu die
   Startadresse eines belegten Blocks und den Namen entgegen. */



void mmdump();
/* Gibt die aktuelle Belegung des Heaps in folgendem Textform auf stdout aus. */

/************|-----------------|*************
*************| Beispielausgabe |*************
*************|-----------------|*************
*   Heapsize: 10000                         *
*   Belegte Blöcke: 3 (1700 Byte / 17%)     *
*   Freie Blöcke: 2 (8300 Byte / 83%)       *
*                                           *
*   Startadresse    Größe   Status  Name    *
*   0x0000:0000     1200    frei    -       *
*   0x0000:1200     1000    belegt  P2      *
*   0x0000:2200     500     belegt  P3      *
*   0x0000:2700     3000    frei    -       *
*   0x0000:5700     200     belegt  P5      *
*   0x0000:5900     4100    frei    -       *
********************************************/
#endif // MM_H_
