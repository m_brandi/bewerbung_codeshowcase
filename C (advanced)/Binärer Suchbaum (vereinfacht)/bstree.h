/*
 * file: bstree.h
 *
 * date: 2021-06-13
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */
 
#ifndef BSTREE_BSTREE_H
#define BSTREE_BSTREE_H
typedef struct bnode{
    int value;
    struct bnode *left, *right;
}BNODE;
void insert(BNODE *root, int val);
BNODE *search(BNODE *root, int val);
BNODE *create_tree(int val);
void inorder_print(BNODE *tree);
void postorder_print(BNODE *tree);
void preorder_print(BNODE *tree);
BNODE *createTreeFromArray(char *ar, int len, int root);
BNODE *searchForOpt(BNODE *tree, int val);
#endif //BSTREE_BSTREE_H
