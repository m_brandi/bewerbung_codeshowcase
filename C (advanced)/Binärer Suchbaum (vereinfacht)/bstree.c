/**
 * file: bstree.c
 *
 * date: 2021-06-13
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */
 
#include "bstree.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

BNODE *create_tree(int val){
    if(val > INT_MAX || val < INT_MIN){
        fprintf(stderr, "Value not in Integer range!");
        return NULL;
    }
    BNODE *tree = malloc(sizeof(struct bnode)) ;
    tree -> value = val;
    tree -> left = NULL;
    tree -> right = NULL;
    return tree;
}

void insert(BNODE *root, int val){
    if(NULL == root ) {
        fprintf(stderr, "Parameter 'root' not found!\n");
        return;
    }
    if(val > INT_MAX || val < INT_MIN){
        fprintf(stderr, "Value not in Integer range!");
        return;
    }
    if(val > root -> value){
        if(NULL == root -> right){
            BNODE *b = create_tree(val);
            root -> right = b;
        }else {
            insert(root -> right, val);
        }
    }
    else{
        if(NULL == root -> left){
            BNODE *b = create_tree(val);
            root -> left = b;
        }else {
            insert(root -> left, val);
        }
    }
}
BNODE *search(BNODE *root, int val){
    if(NULL == root ) {
        fprintf(stderr, "Parameter 'Root' could not be found!");
        return NULL;
    }
    if(val > INT_MAX || val < INT_MIN){
        fprintf(stderr, "Value not in Integer range!");
    }
    if(val == root -> value)
        return root;
    if(val < root -> value) {
        if(root->left != NULL)
            return search(root->left, val);
        else{
            return NULL;
        }
    }
    if(val > root -> value) {
        if(root->right != NULL)
            return search(root->right, val);
        else{
            return NULL;
        }
    }
}

void inorder_print(BNODE *node){
    if(node == NULL){
        return;
    }
    inorder_print(node->left);
    printf("%d\t", node -> value);

    inorder_print(node -> right);
}
void postorder_print(BNODE *node){
    if(node == NULL){
        return;
    }
    postorder_print(node->left);
    postorder_print(node -> right);
    printf("%d\t", node -> value);

}
void preorder_print(BNODE *node){
    if(node == NULL){
        return;
    }
    printf("%d\t", node -> value);
    preorder_print(node->left);
    preorder_print(node -> right);
}

BNODE *createTreeFromArray(char *ar, int len, int root){
    int root_check = 0;
    BNODE *tree = create_tree(root);
    for(int i = 0; i < len; i++){
        if(*(ar+i) == root){
            root_check = 1;
        }
        if(*(ar+i) > 0) {
            insert(tree, *(ar + i));
        }
    }
    if(root_check == 0){
        fprintf(stderr, "Parameter 'root' must be an existing value in the list!\n");
        return NULL;
    }
    return tree;
}
BNODE *searchForOpt(BNODE *tree, int val){
    if(tree == NULL){
        fprintf(stderr, "Parameter 'tree' could not be found\n");
    }
    if(val > INT_MAX || val < INT_MIN){
        fprintf(stderr, "Value not in Integer range!\n");
        return NULL;
    }
    BNODE *opt_node = tree;
    BNODE *prev_node = malloc(sizeof(BNODE));
    while(opt_node != NULL) {
        if(opt_node->value == val){
            return opt_node;
        }
        if(opt_node->value < val){
            prev_node = opt_node;
            opt_node = opt_node -> right;
        }
        else{
            prev_node = opt_node;
            opt_node = opt_node -> left;
        }
    }
    if(prev_node -> value > val){
        return prev_node;
    }
    else{
        fprintf(stderr, "Could not find enough space!\n");
    }
}
