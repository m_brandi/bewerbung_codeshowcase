/*
 * file: list.c
 *
 * date: 2021-04-28
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#include <stdio.h>
#include "list.h"
#include <stdlib.h>

LINK* create_list(){
    LINK *header = (LINK*) malloc(sizeof(struct link));   

    header -> value = 0;
    header -> left = NULL;
    header -> right = NULL;
    return header;
}
void insert(double val, LINK *list){
    if(list == NULL)
        return;
    LINK *tail = list -> right;

    LINK *new_link = (LINK*) malloc(sizeof(struct link));
    new_link -> left = NULL;
    new_link -> right = NULL;
    new_link -> value = val;

    if(tail == NULL) {
        list->right = new_link;
        list->left = new_link;
        list -> value++;
        return;
    }
    tail -> right = new_link;
    list -> value++;
    new_link -> left = tail;
    list -> right = new_link;
    return;
}
LINK* search(double val, LINK *list){
    if(list == NULL)
        return NULL;
    LINK *link = list -> left;
    while(link != NULL){
        if(link -> value == val)
            return link;
        link = link -> right;
    }
    return NULL;
}
void delete_by_val(double val, LINK *list){
    if(list == NULL)
        return;
    LINK* link = search(val, list);
    if(link == NULL){
        return;
    }
    ultimate_delete(link, list);
}
void delete_by_pos(int pos, LINK* list){
    if(list == NULL)
        return;
    if(pos <= 0)
        return;
    LINK* link = list -> left;
    for(int i = 1; i < pos; i++){
        link = link -> right;
    }
    ultimate_delete(link, list);
}
void ultimate_delete(LINK* link, LINK *list){
    if(list == NULL)
        return;

    if(link == NULL){
        return;
    }
    if(link->left != NULL){
        link ->left -> right = link -> right;
    }
    else{
        list -> left = link -> right;
    }
    if(link -> right != NULL){
        link -> right -> left = link -> left;
    }
    else{
        list -> right = link -> left;
    }
    free(link);
    link = NULL;
    list -> value--;
}
double* traverse(LINK* list){
    if(list == NULL)
        return NULL;
    int len = list -> value;
    double* ar = (double*) calloc(len, sizeof(double));
    LINK* link = list -> left;
    for(int i = 0; i < len; i++){
        ar[i] = link -> value;
        link = link -> right;
    }
    return ar;
}
void delete_list(LINK *list){
    if(list == NULL)
        return;
    int len = list -> value;
    if(len > 0) {
        for (int i = 1; i <= len; i++) {
            delete_by_pos(1, list);
        }
    }
    free(list);
    list = NULL;
}
