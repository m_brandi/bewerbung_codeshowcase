/*
 * file: list.h
 *
 * date: 2021-04-28
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#ifndef CHAINED_LIST_H
#define CHAINED_LIST_H

typedef struct link{
    double value;
    struct link *left, *right;
}LINK;
LINK* create_list();
void insert(double val, LINK *list);
LINK* search(double val, LINK *list);
void delete_by_val(double val, LINK *list);
void delete_by_pos(int pos, LINK *list);
void ultimate_delete(LINK *link, LINK *list);
double* traverse(LINK *list);
void delete_list(LINK *list);

#endif //CHAINED_LIST_H
