/*
 * file: bstree.c
 *
 * date: 2021-06-30
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#include "bstree.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

BNODE *create_tree(int val){
    if(val > INT_MAX || val < INT_MIN){
        fprintf(stderr, "Value not in Integer range!");
        return NULL;
    }
    BNODE *tree = malloc(sizeof(struct bnode)) ;
    tree -> value = val;
    tree -> left = NULL;
    tree -> right = NULL;
    return tree;
}

void insert_into_tree(BNODE *root, int val){
    if(NULL == root ) {
        fprintf(stderr, "Parameter 'root' not found!\n");
        return;
    }
    if(val > INT_MAX || val < INT_MIN){
        fprintf(stderr, "Value not in Integer range!");
        return;
    }
    if(val > root -> value){
        if(NULL == root -> right){
            BNODE *b = create_tree(val);
            root -> right = b;
        }else {
            insert_into_tree(root -> right, val);
        }
    }
    else{
        if(NULL == root -> left){
            BNODE *b = create_tree(val);
            root -> left = b;
        }else {
            insert_into_tree(root -> left, val);
        }
    }
}
BNODE *search_in_tree(BNODE *root, int val){
    if(NULL == root ) {
        fprintf(stderr, "Parameter 'Root' could not be found!");
        return NULL;
    }
    if(val > INT_MAX || val < INT_MIN){
        fprintf(stderr, "Value not in Integer range!");
    }
    if(val == root -> value)
        return root;
    if(val < root -> value) {
        if(root->left != NULL)
            return search_in_tree(root->left, val);
        else{
            return NULL;
        }
    }
    if(val > root -> value) {
        if(root->right != NULL)
            return search_in_tree(root->right, val);
        else{
            return NULL;
        }
    }
}
void postorder_print(BNODE *node){
    if(node == NULL){
        return;
    }
    postorder_print(node->left);
    postorder_print(node -> right);
    printf("%d\t", node -> value);
}
