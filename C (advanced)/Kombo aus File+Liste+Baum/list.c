/*
 * file: list.c
 *
 * date: 2021-06-30
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */

#include <stdio.h>
#include "list.h"
#include <stdlib.h>

LINK* create_list(){
    LINK *header = malloc(sizeof(struct link));

    header -> age = 0;
    header -> left = NULL;
    header -> right = NULL;
    return header;
}
void insert_into_list( int val, char* nam, LINK *list){
    if(list == NULL)
        return;
    LINK *tail = list -> right;

    LINK *new_link = malloc(sizeof(struct link));
    new_link -> left = NULL;
    new_link -> right = NULL;
    new_link -> age = val;
    new_link -> name = nam;
    if(tail == NULL) {
        list->right = new_link;
        list->left = new_link;
        list -> age++;
        return;
    }
    tail -> right = new_link;
    list -> age++;
    new_link -> left = tail;
    list -> right = new_link;
    return;
}
int* traverse(LINK* list){
    if(list == NULL)
        return NULL;
    int len = list -> age;
    int* ar = calloc(len, sizeof(int));
    LINK* link = list -> left;
    for(int i = 0; i < len; i++){
        ar[i] = link -> age;
        link = link -> right;
    }
    return ar;
}
