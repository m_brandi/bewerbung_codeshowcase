/**
 * file: bstree.h
 *
 * date: 2021-06-30
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#ifndef BSTREE_BSTREE_H
#define BSTREE_BSTREE_H

typedef struct bnode{
    int value;
    struct bnode *left, *right;
}BNODE;

void insert_into_tree(BNODE *root, int val);
BNODE *search_in_tree(BNODE *root, int val);
BNODE *create_tree(int val);
void postorder_print(BNODE *tree);


#endif //BSTREE_BSTREE_H
