/**
 * file: main.c
 *
 * date: 2021-06-30
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "bstree.h"

#define file_size 100
typedef struct data{
    char *name;
    int age;
}DATA;
LINK *liste;
BNODE *tree;
int amount_of_data = 1;

DATA *read_File(char *file_name){
    FILE *fp = fopen(file_name, "rb");
    if(fp == NULL){
        fprintf(stderr, "Fehler beim Oeffnen der Datei!\n");
        return NULL;
    }
    DATA *content = malloc(sizeof(DATA)*file_size);
    if(content == NULL){
        fprintf(stderr, "Fehler bei Speicheralokierung!\n");
        return NULL;
    }
    amount_of_data = fread(content, sizeof(DATA), file_size, fp);
    return content;
}
void insert_to_list(DATA *d, int amount){
    for(int i = 0; i < amount; i++){
        insert_into_list(d[i].age, d[i].name, liste);
    }
}
void create_search_index(){
    int *ar = traverse(liste);
    for(int i = 0; i < amount_of_data; i++){
        insert_into_tree(tree, ar[i]);
    }
}

int main(int argc, char *argv[]){
    liste = create_list();
    tree = create_tree(45);  //Durchschnittsalter bei 45 Jahre
    DATA *daten = read_File(argv[1]);
    insert_to_list(daten, amount_of_data);
    create_search_index();
    postorder_print(tree);
}
