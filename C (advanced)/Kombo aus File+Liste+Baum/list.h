/**
 * file: list.h
 *
 * date: 2021-06-30
 * author: Marco Brandauer
 * email: marcobrandauer44@gmail.com
 */


#ifndef CHAINED_LIST_H
#define CHAINED_LIST_H

typedef struct link{
    int age;
    char *name;
    struct link *left, *right;
}LINK;
LINK* create_list();
void insert_into_list(int val, char *nam, LINK *list);
int* traverse(LINK *list);

#endif //CHAINED_LIST_H
