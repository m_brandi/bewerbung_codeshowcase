package main_package;
import java.util.Scanner;
import java.util.Random;

public class Lotto6aus45 {

	public static void main(String[] args) {
		
		Scanner inp = new Scanner(System.in);
		Random rand = new Random();
		int guess[] = new int[6], los[] = new int[6];
		
		System.out.println("Willkommen zu Lotto!");
		System.out.println("Geben Sie ihre Ziehung ein: ");
		
		for(int i = 0; i < 6; i++) {
			guess[i] = inp.nextInt();
			los[i] = rand.nextInt(45)+1;
		}
		
		System.out.print("Ihre Ziehung: ");
		for(int i = 0; i < 6; i++) {
			System.out.print(guess[i] + " ");
		}
		System.out.print("\nDie heutige Ziehung: ");
		for(int i = 0; i < 6; i++) {
			System.out.print(los[i] + " ");
		}
	}

}
