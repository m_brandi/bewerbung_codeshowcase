package main_package;
import java.util.Scanner;

public class XChiffText {

	public static void main(String[] args) {
		
		char[] key = {1,2,3,4,5,6,7,8};
		String codew;
		Scanner inp = new Scanner(System.in);
		
		System.out.println("Geben Sie einen Text ein: ");
		codew = inp.nextLine();
		System.out.println("Der verschlüsselte Text: ");
		String v_text = verschlüsseln(key, codew);
		System.out.println(v_text);
		System.out.println("Der entschlüsselte Text: ");
		System.out.println(entschlüsseln(key, v_text));
		
	}
	
	public static String verschlüsseln(char[] key, String text) {
		char[] chiff_text = text.toCharArray();
		for(int i = 0; i < text.length(); i++) {
			chiff_text[i] += key[i%8];
		}
		String ver_n = new String(chiff_text);
		return ver_n;
	}
	public static String entschlüsseln(char[] key, String text) {
		char[] dech_text = text.toCharArray();
		for(int i = 0; i < text.length(); i++) {
			dech_text[i] -= key[i%8];
		}
		String ent_n = new String(dech_text);
		return ent_n;
	}

}
