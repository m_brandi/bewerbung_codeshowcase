package main_package;
import java.util.Scanner;

public class i2bin {

	public static void main(String[] args) {
		
		Scanner inp = new Scanner(System.in);
		int zahl, i = 0, f = 0;
	    int bin[] = new int[1000];
	    boolean is_div = true;


	    System.out.println("Geben Sie eine Zahl ein: ");
	    zahl = inp.nextInt();

	    while (is_div == true){
	        if(zahl < Math.pow(2, f)){
	            is_div = false;
	        }
	        f++;
	    }
	    
	    if(zahl == 0){
	        bin[0] = 0;
	        f = 2;
	    }

	    while(zahl != 0){

	        bin[i] = zahl % 2;
	        zahl /= 2;
	        i++;
	    }
	    
	    
	    System.out.println("In bin�rer Darstellung: ");

	    while(f-1 > 0){

	    System.out.print(bin[f-2]);
	        f--;
	    }


	}

}
