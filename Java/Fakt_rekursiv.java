package main_package;

import java.util.Scanner;

public class Fakt_rekursiv {

public static void main(String[] args) {
		
		Scanner inp = new Scanner(System.in);
		int f = 0;
		
		System.out.println("Von welcher Zahl m�chten Sie die Faktorielle erhalten");
		f = inp.nextInt();
		System.out.println("Die Faktorielle von " + f + " ist " + fact(f));
		
	}
	
	public static int fact(int f) {
		
		if(f>1) {
			f *= fact(f-1);
		}
		return f;
	}

}
