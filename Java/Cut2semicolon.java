package main_package;
import java.util.Scanner;

public class Cut2semicolon {

	public static void main(String[] args) {
		
		Scanner inp = new Scanner(System.in);
		
		System.out.println("Geben Sie den zu filternden Text ein");
		
		String text = inp.nextLine();
		int pos = text.indexOf(';');
		String text2 = text.substring(pos+1);
		int pos2 = text2.indexOf(';');
		String s_text = text2.substring(0, pos2);
		
		System.out.println("Gefilterter Text: " + s_text);
	}	
}
