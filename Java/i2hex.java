package main_package;

import java.util.Scanner;

public class i2hex {

	public static void main(String[] args) {
		
		Scanner inp = new Scanner(System.in);
		int zahl, i = 0, f = 0;
	    char bin[] = new char[1000];
	    char a;
	    boolean is_div = true;


	    System.out.println("Geben Sie eine Zahl ein: ");
	    zahl = inp.nextInt();

	    while (is_div == true){
	        if(zahl < Math.pow(16, f)){
	            is_div = false;
	        }
	        f++;
	    }
	    
	    if(zahl == 0){
	        bin[0] = 0;
	        f = 2;
	    }

	    while(zahl != 0){

	        bin[i] = (char) (zahl%16);
	        switch(zahl%16) {
	        
	        case 10:
	        	bin[i]= 'A';
	        	break;
	        case 11:
        		bin[i]= 'B';
        		break;
	        case 12:
        		bin[i]= 'C';
        		break;
	        case 13:
        		bin[i]= 'D';
        		break;
	        case 14:
        		bin[i]= 'E';
        		break;
	        case 15:
        		bin[i]= 'F';
        		break;
	       	        	
	        }
	        zahl /= 16;
	        i++;
	    }
	    
	    
	    System.out.println("In hexadezimaler Darstellung: ");

	    while(f-1 > 0){

		    if( (int) (bin[f-2]) >= 0 && (int) (bin[f-2]) < 10) {
		    	System.out.print( (int) bin[f-2]);
		        f--;
		    }
		    else{
		    	System.out.print(bin[f-2]);
		        f--;
		    }
	    }



	}

}
